
$(document).ready(function(){
   $("#toggle").click(function() {
      $(this).toggleClass("on");
      $("#toggle + nav").slideToggle();
      $('body').toggleClass("menu-open");
   });
   
   $('.slick-carousel').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      arrows: false,
    });   
});